---
tags:
  - 2022
---

# 3. Bhubaneswar to Allahabad

<hr/>
{% for key, value in bbsr_alld.items() %}
{% if key == 'hotels' %}
-  Overnight halts: {{value}}
{% elif key == 'pitstops' %}
-  Road breaks: {{value}}
{% else %}
-  {{value}}
{% endif %}
{% endfor %}
<hr/>

The dates were convenient, it didn't rain the previous day, and rooms were available at my preferred hotel.

Here's the odometer readings for the jouney.

| Day 1 start | Day 1 end, Day 2 start | Day 3 end                   |
| --- | --- |-----------------------------|
| ![](../images/IMG_0718.jpg)|![](../images/IMG_0720.jpg)| ![](../images/IMG_0722.jpg) |
| 14,748 km | 15,234 km | 15,862 km                   |


## Day 1: Bhubaneswar - Durgapur

Distance 506 km, time: 11 hours

![route Bhubaneswar Durgapur](../images/bbsr_dgr.png)

Several diversions due to road widening work. The diversions are broken roads with newly constructed horrible speedbumps, and with water-filled potholes that you can't see till you're about an inch away (and then, it's too late to do anything). You'll be downshifting for almost 30% of this route.

If you're leaving very early in the morning, while in Odisha it's advisable to drive on the middle of the road because the dividers are full of (invisible) people plucking flowers from the trees on the divider. Once there's sunlight, it's advisable to use the leftmost lane because cattle randomly occupy the rightmost lane (and eat the grass on the dividers) or the middle lane (where they snooze).

Once you cross over to Bengal, no cattle on roads. However, the stretch after Bishnupur is through villages that have stray poultry and goats roaming around the narrow village roads.

After Bishnupur, around Joykrishnapur, GoogleMaps directed me to a mud-tracked, slushy road where a car and a bicycle can't pass each other without a lot of manouvering. This was for almost 10 - 15 kilometers (includes about 7 kilometers of completely deserted jungle area). GoogleMaps called it the Bishnupur-Sonamukhi-someMisPronouncedName road. Extremely unpleasant and somewhat scary experience.

!!! tip "Alternate route"

    On a subsequent trip, from Kharagpur, I did not proceed to Salboni or Chandrakona Road. Instead, I went to Dankuni and thence through Burdwan to Durgapur.
	
	This route completely avoids the jungles and 10 - 15 km of narrow mud track that passes as a road on the Bishnupur - Durgapur stretch. About 120 km extra but all highway roads.

There were several toll booths in Bengal but, except for the one at Rampura, none of them had the toll barriers down, so no Fastag deductions at all for almost the entire state.

!!! tip "Best part of this stretch"

    The Durgapur barrage. It was raining quite heavily by the time I pulled into Durgapur, and I could see the Damodar river in full spate. It was awe-inspiring.

## Day 2: Durgapur - Allahabad

Distance 650 km, time: 14 hours

![route Durgapur Allahabad](../images/dgr_alld.png)

Again, several diversions due to road widening work but once you enter Bihar, the diversions are tarred and smooth. On the whole, this entire stretch is nice driving, and once you cross over to UP after the Chandauli toll, it's one long elevated expressway. Driving on this expressway was a pleasure (esp. because I remember the earlier Allahabad - Varanasi commutes through the congested roads).

I lost almost 20 minutes at the Daffi toll plaza due to a pile-up of trucks. In contrast, the infamous Sasaram toll plaza was a breeze.

I lost another 30 minutes when it started raining heavily after I left the expressway to get on to the Allahabad G.T.Road (old) at Handia because of waterlogging and generally narrow roads (and invisible potholes). I used a mini-truck in front of me as a "sweeper" vehicle to avoid the potholes, but I'm afraid I might have sprayed a lot of cyclists and motorcyclists with rainwater. My car itself was sprayed over by passing buses and trucks, which means I lost visibility for 2-3 seconds every time this happened (despite the windscreen wiper going full throttle). This last 45 minutes was the most unpleasant part of this stretch, and it wasn't until I crossed the bridge over the Ganga at Allahabad that I could breathe easier.

!!! tip "Best part of this stretch"

    -  Driving through places I'd read of in storybooks as a child: Maithon, Giridih, Topchanchi.
    -  The Sone river at full spate at Dehri. 
    -  The Karamnasa river at full spate at Chandauli. 
    -  The Ganges at full spate at Allahabad.

## Expenses

| | Expenses (INR) |
|---|---------------:|
| Petrol |       6,206.96 |
| Toll |       1,285.00 |
| Hotel |       2,010.00 |
| Food |         132.00 |
| **Total** |   **9,633.96** |

<!--
September 23 - 24, 2022
Allahabad to Bhubaneswar
Day 1: Allahabad to Duragpur. 4:30 AM to 7:00 PM.
Day 2: Duragpur to Bhubaneswar via Dankuni. 4:30 AM to 8:00 PM.
Odometer: 15962 - 17188
Petrol: 8205.70
Toll: 1,815.00
Hotel: 2,234.00
Food: 393.00
Total: 12,647.00

-->

<hr/>