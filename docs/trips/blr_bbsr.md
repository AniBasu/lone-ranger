---
tags:
  - 2022
---

# 2. Bangalore to Bhubaneswar

<hr/>
{% for key, value in blr_bbsr.items() %}
{% if key == 'hotels' %}
-  Overnight halts: {{value}}
{% elif key == 'pitstops' %}
-  Road breaks: {{value}}
{% else %}
-  {{value}}
{% endif %}
{% endfor %}
<hr/>

A long, interstate solo car trip in the middle of the worst heatwave in human memory is not a good idea. But, the dates were convenient, so I went.

![route](../images/blrbbsr/blr_bbsr_route_nh16.png)

## Day 1: Bangalore - Vijaywada

I covered  almost 700 km, and my average speed was 52 kph. The graph shows the speed reckoned from the distance between the several toll plazas along the route (and the timestamp on my FastTag deductions).

![graph day 1](../images/blrbbsr/day1_3.png)

The route was Bangalore - Hoskote - Tirupati - Nellore - Ongole - Vijaywada. 

The roads were deserted. It was blazing hot :octicons-flame-16:{ .red }; the tar shimmered and was, at places, melting. I read in the newspapers later that it had been past 45 degrees in all these places in Andhra that day. That day, my phone saw red (see [Learnings](ready_to_go.md#mobile-holders-on-dashboards)).

![iphone alert](../images/blrbbsr/iPhone_alert.png)

And then, about 20 km from my hotel, the rear right tyre gave up. It just collapsed upon itself. I got out to look (the car was wobbling so, yeah, I had to check). The poor tyre had such a defeated look about it, all crumpled and battered. But it was still game enough for me to drive to the nearest roadside puncherwalla, where a teenager took it out, affixed the stepney, and advised me to get an 'expert' opinion once I reach Vijaywada.

!!! tip "Scared"

    No, I don't have pictures of the burst tyre, the helpful teenaged mechanic, or the shop. I was too rattled to think about future blog posts. 

But, I was too tired by the time I reached Vijaywada. So, I ate curd rice, and slept.

Here are the distance readings for Day 1.

| Day 1 start                          | Day 1 end                            |
|--------------------------------------|--------------------------------------|
| ![](../images/blrbbsr/IMG_0629.jpeg) | ![](../images/blrbbsr/IMG_0630.jpeg) |
| 12,947 km                            | 13,623 km                            |

## Day 2: Vijaywada - Vishakhapatnam

The sleep had reactivated my brain. 

> "Your other 3 tyres are as old as the busted one", it whispered. "What if they too burst?"

> "True", I mused. "It's going to be equally hot today."

> "But we have just half the distance of yesterday's to cover. Let's risk it."

So there I was, at 5:00 in the morning, patting my car and whispering, "We'll make it, okay?" (Normal people pray to their gods; agnosts like me talk to inanimate objects like cars. And laptops.)

The security guard sauntered over and asked something. I understood only one word: _luggage_. I smiled and pointed inside. Then, I pointed to the exit and said, _Slope_ and made a face. 

!!! tip "Bête noire"

    Friends know of my fear of driving on slopes. The experience at Jubilee Hills/Banjara Hills, Hyderabad (and especially that left turn from the old Mumbai highway that connects Shaikpet to Road No. 51) is not something I'd care to repeat. 

The guard regarded the slope, let out a barrage of sentences (in which I recognised two words: _first gear_), and made a sweeping upwards aeroplane-like motion :material-airplane-check:{ .blue }. I gulped, nodded, got in, drove over the slope in first gear, turned back, and wave. I was off.

That day, I drove slower than my usual. The average speed was 40 kph that day. The highlight of the day was spotting a [Cafe Coffee Day](https://goo.gl/maps/QoX941HT3Et8e3DC6) on the highway. (It was the only CCD I saw the entire Bangalore-Bhubaneswar stretch. What a shame!) I spent almost 30 minutes there, cooling down with a dark frappe.

![graph day 2](../images/blrbbsr/day2_1.png)

It was still afternoon when I reached the hotel. I'd driven through Eluru and Annavaram.

|Day 2 start|Day 2 end|
|---|---|
|![](../images/blrbbsr/IMG_0630.jpeg)|![](../images/blrbbsr/IMG_0631.jpeg)|
|13,623 km|13,997 km|

After that nerve-wracking drive, I needed my comfort food. It wasn't on the menu, but the smiling hostess promised she'll get it within 15 minutes, bless her heart. After that bread-omlette, I walked to a car workshop nearby (I'd seen it on my way to the hotel.)

> "We do only premium cars madam", they said apologetically.

Mine was very definitely a non-premier car. They advised me to check with the Ford workshop about 3 km away.

I could've called the workshop. For some reason, I didn't; I took an Uber. Good that I didn't call, because of what happened next.

> "Yes, madam, checkup for roadworthiness we'll do. Yes madam, stepney also we'll check and replace tyres if required."

> "I'll bring the car tomorrow at what time", I asked.

> "Tomorrow, closed madam. Ramzan."

Whoa! What now? The workshop was closing in less than an hour, and I wouldn't be able to get my car there by that time. I explained to them I needed to leave Vizag early the day after. They realised I was an out-of-towner, and sent a machanic with me to the hotel. He did a visual roadworthiness check (brake oil, engine oil, battery, coolant, and all), took me and the car to a tyre shop, and supervised the replacement of all 4 tyres.

(Thank you :material-hands-pray:{ .red } [PPS Ford, Maddilapalem, Vizag](https://goo.gl/maps/8cSriUTtxsA5y8hq8).)

## Day 3: Vishakhapatnam

With the car fixed, I could've left this day. But, I'd already asked the hotel to book me for another day. So, on the 3rd day, I rested. I read newspapers, ate sandwiches, snoozed, and watched the waves crashing on the beach.

I couldn't make out the difference between high tide and low tide waves, though.

![tide tables](../images/blrbbsr/day3_3.png)

## Day 4: Vishakhapatnam - Bhubaneswar

Ah, the last leg of the journey. Searing dry heat had changed to less hot and more humid climes.

![NH 16](../images/blrbbsr/IMG_0636.jpeg)

That day, I drove about 430 km and took 8.5 hr to do so. Road-widening work meant, every 10 km or so, I'd get a 2 km stretch of hobbled roads. I guess in another year or so, driving on this stretch would be a breeze. I drove through Berhampore, Gopalpur, and Ganjam.

![graph last day 1](../images/blrbbsr/day3_1.png)

I drove through elephant corridors, circled Chilka, and before I knew it, was home and dry, and getting myself a pizza through Zomato.

| Last day start                       |Last day end|
|--------------------------------------|---|
| ![](../images/blrbbsr/IMG_0633.jpeg) |![](../images/blrbbsr/IMG_0638.jpeg)|
|14,001 km|14,424 km|

## Expenses

| | Expenses (INR) |
|---|---------------:|
| Petrol | 11,837.51 |
| Toll | 1,615.00|
| Hotel | 19,564.79 |
| Food | 2,076.92 |
| **Total** | **35,094.22** |

Had I not spent that extra day at Vizag, the hotel bill would've been about 8k lesser, but I guess everyone needs a free rest day now and then!

<hr/>