---
tags:
  - 2023
---

# 1. Hyderabad to Bangalore

<hr/>
{% for key, value in alld_ayodhya.items() %}
{% if key == 'hotels' %}
-  Overnight halts: {{value}}
{% elif key == 'pitstops' %}
-  Road breaks: {{value}}
{% else %}
-  {{value}}
{% endif %}
{% endfor %}
<hr/>

The intention was to do an Ayodhya - Faizabad - Lucknow trip, but it ended up as only a see-Ram-Lalla trip.

This trip has no pictures. There was nothing noteworthy to point at and click. And, photography is prohibited inside the Ram Janambhoomi temple.

## Observations

The road is two-laned for the most part, and often without dividers. It also passes through two fairly active rail tracks. 

Road conditions are good: no potholes, no diversions, old-fashioned highway and, therefore, old shade trees on either side.  However, cruising speeds are not possible because it's a fairly active road, and only two-laned.

Petrol pumps are few and far between.

<hr/>