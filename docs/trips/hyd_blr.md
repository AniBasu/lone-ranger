---
tags:
  - 2020
---

# 1. Hyderabad to Bangalore

<hr/>
{% for key, value in hyd_blr.items() %}
{% if key == 'hotels' %}
-  Overnight halts: {{value}}
{% elif key == 'pitstops' %}
-  Road breaks: {{value}}
{% else %}
-  {{value}}
{% endif %}
{% endfor %}
<hr/>

During the Covid-induced pandemic, I changed jobs, and then moved cities. I sent my books forward to Bangalore through a logistics company, threw my papers and clothes into my car, and drove from Hyderabad to Bangalore.

It took me 12 hours door-to-door (4:38 AM to 4:47 PM). People routinely do this stretch in 8-9 hours, but I drive slow, what else can I say.

![NH44](../images/hyd_blr.jpg)

## Observations

-  Toll plaza, cash-only line is at the extreme left for Telangana, and the rightmost for Andhra and Karnataka.
-  Andhra has totally BS speed-breakers throughout the highway. For reasons best known only to its road department.
-  Take a 5-minute break near the Kia plant at Anantapur to admire the windmills atop the hillocks.

<hr/>