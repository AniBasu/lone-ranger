# Safarnama

<hr/>

I love the road, and I love to drive. It was when I drove myself during the Covid pandemic from one city to another for a relocation that I thought, why not do a `pan-India driving project`? Hence this blog, which is intended as a chronicle of the trips in this project.

## Trips

These are women-only drives where I'm either alone or have my mother as the sole passenger. The guidelines that I follow are:

-  Aim to reach the destination before sundown.
-  Share the live location with family and at least two trusted friends.
-  Cover no more than 500 km in a day. Give or take a 100.

Here's the running tally:

|        | Map pins                | Distance (km) | Days | Route               |
|--------|-------------------------|---------------|------|---------------------|
| Trip 1 | Hyderabad - Bangalore   | 589           | 1    | NH 44               |
| Trip 2 | Bangalore - Bhubaneswar | 1474          | 4    | NH 16               |
| Trip 3 | Bhubaneswar - Allahabad :octicons-sync-24:{ .maroon } :fontawesome-solid-user-group:{ .maroon }| 1156 | 2 | NH 16, NH 14, NH 19 |
| Trip 4 | Allahabad - Ayodhya :fontawesome-solid-user-group:{ .maroon }| 170 | 1 | NH 330, NH 27 |
|        || **3389**               | **8**         ||

-  :octicons-sync-24:{ .maroon } These trips were done more than once.
-  :fontawesome-solid-user-group:{ .maroon } These trips had my mom as a passenger. 

The following map is an indication of the route coverage.

![route Hyd Blr Bbsr Alld](./images/pan_India.png)

<!--

## Mini breaks

The short breaks from any of the trip points are called `mini breaks`. These are not always solo trips, and aren't meant to be reckoned for the `pan-India driving project`. To visualise, think of the project as a wheel. The trips are points on the diameter. The detours are spokes from these trips that may or may not form another smaller circle with its radius on the project-wheel-diameter.

![trips and breaks diagram](./images/trips_and_breaks.png)

-->

## Car

It's a 2012 Ford Figo Petrol 1.2.

| At my favouritest place in Hyderabad | At my favouritest place in Bhubaneswar | 
| --- | --- |
| ![Car at the Seven Tombs necropolis, Hyderabad](./images/DSCN0623.JPG) | ![Car at Jaydev Vatika, Bhubaneswar](./images/IMG_1009.jpeg) |
| At the Seven Tombs necropolis | At Jaydev Vatika |

The car has these at all times:

{% include 'common/things_to_carry_always.md' %}

Additionally, during trips, it carries:

{% include 'common/things_to_carry_trips.md' %}

At the start of the project, the car had no extra fittings and furnishings except for scuff plates and a bespoke cloth seatcover. Oh, and it also had/has this cushion for driver comfort.

![Ravenclaw cushion cover](./images/IMG_1144.jpeg)


Thereafter, it aquired a few more things, listed here chronologically:

-  A dashboard mobile holder. But, see [Learnings](trips/ready_to_go.md#mobile-holders-on-dashboards).
-  Ceat 185/65 R14 86T tyres, as replacement for the original tyres (Goodyear, 175/65 R14), because of a tyre burst on the way (See [Day 2, BZA - VSKP](trips/blr_bbsr.md#day-2-vijaywada-vishakhapatnam)).
-  12V 100/90W headlight bulbs, as replacement for the original headlight bulbs (12V 60/55W) that were inadequate on highways before sunrise. I'm aware that prolonged use might melt the holders, but I reckoned I wouldn't be using them for more than 2 hours continuously.
-  Window shades, for privacy purposes.
-  Cushions for lumbar support and back support, for comfort of parent
	![Car at Jaydev Vatika, Bhubaneswar](./images/IMG_1143.jpeg)
-  Bolster for neck support, for comfort of parent
-  Rolled-up car cover stashed beneath the glove compartment, as footstool for parent

## Me

I was born and raised in Allahabad, and now spend my time between Bangalore, Bhubaneswar, and Allahabad. I've also lived at Ranchi, Delhi, Bombay, and Hyderabad.

[More about me](https://registry.jsonresume.org/AninditaBasu)

<hr/>
